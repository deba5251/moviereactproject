// import logo from './logo.svg';
import React, { Component } from 'react';
import './App.css';
import Dashboard from './components/dashboard';

import 'bootstrap/dist/css/bootstrap.min.css';
class App extends Component {
  render() {
    return (
      <div>
        <Dashboard />
      </div>
    );
  }
}

export default App;
